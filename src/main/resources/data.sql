create schema public
;

comment on schema public is 'standard public schema'
;

create table if not exists users
(
  name text not null,
  surname text not null,
  email text not null,
  password text not null,
  id serial not null
    constraint users_pk
    primary key,
  confirmed boolean default false not null,
  registration_date timestamp not null
)
;

create table if not exists authorities
(
  role text not null,
  id serial not null
    constraint authorities_pk
    primary key
)
;

create table if not exists users_autohorities
(
  user_id integer not null
    constraint users_autohorities_users_id_fk
    references users
    on update cascade on delete cascade,
  authority_id integer not null
    constraint users_autohorities_authorities_id_fk
    references authorities
)
;

create table if not exists products
(
  id serial not null
    constraint products_pkey
    primary key,
  name text not null,
  price integer not null,
  description text not null,
  image_uri text not null,
  full_image_uri text not null
)
;

create table if not exists users_products
(
  id serial not null
    constraint users_products_pkey
    primary key,
  product_id integer not null
    constraint users_products_products_id_fk
    references products,
  count integer default 1 not null,
  user_id integer not null
    constraint users_products_users_id_fk
    references users
    on update cascade on delete cascade
)
;

create table if not exists users_orders
(
  id serial not null
    constraint users_orders_pkey
    primary key,
  user_id integer not null
    constraint users_orders_users_id_fk
    references users
    on update cascade on delete cascade
)
;

create table if not exists orders_products
(
  order_id integer not null
    constraint orders_products_users_orders_id_fk
    references users_orders
    on update cascade on delete cascade,
  product_id integer not null
    constraint orders_products_products_id_fk
    references products
    on update cascade on delete cascade,
  count integer not null,
  total_sum integer not null,
  name text not null
)
;

create table if not exists activation_codes
(
  code text not null,
  user_id integer not null
    constraint activation_codes_users_id_fk
    references users
    on update cascade on delete cascade,
  end_date timestamp not null,
  type text not null,
  id serial not null
    constraint activation_codes_pk
    primary key
)
;

create unique index if not exists activation_codes_code_uindex
  on activation_codes (code)
;

create or replace function delete_non_activated_accounts() returns void
language plpgsql
as $$
BEGIN
  DELETE FROM users WHERE confirmed = false AND registration_date < NOW() - interval '3 days';
END;
$$
;

create or replace function delete_old_activation_codes() returns void
language plpgsql
as $$
BEGIN
  DELETE FROM activation_codes WHERE end_date < NOW();
END;
$$
;
