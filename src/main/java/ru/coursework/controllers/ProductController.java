package ru.coursework.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.coursework.dto.ProductDto;
import ru.coursework.dto.ProductsPaymentDto;
import ru.coursework.exceptions.FormIsNotValidException;
import ru.coursework.models.Product;
import ru.coursework.models.UserOrder;
import ru.coursework.models.UserProduct;
import ru.coursework.services.ProductService;
import ru.coursework.utils.UserUtils;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/product")
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/get/{id}")
    public Product getProduct(@PathVariable int id) {
        return productService.getProduct(id);
    }

    @GetMapping("/all")
    public List<Product> getProducts(@RequestParam(required = false, defaultValue = "false") boolean byPrice,
                                     @RequestParam(required = false, defaultValue = "0") int minPrice,
                                     @RequestParam(required = false, defaultValue = "-1") int maxPrice) {
        return productService.getProducts(byPrice, minPrice, maxPrice);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/add")
    public @ResponseBody int addProduct(@Valid @RequestBody ProductDto productDto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new FormIsNotValidException(bindingResult
                    .getFieldErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList()));
        }
        return productService.addProduct(productDto);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/delete/{id}")
    public void deleteProduct(@PathVariable int id) {
        productService.deleteProduct(id);
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/cart/add/{id}/{count}")
    public void addUserProducts(@PathVariable int id, @PathVariable int count) {
        productService.addUserProduct(id, count, UserUtils.getCurrentUser().getId());
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/cart/update/{id}/{count}")
    public void updateUserProducts(@PathVariable int id, @PathVariable int count) {
        productService.updateUserProduct(id, count, UserUtils.getCurrentUser().getId());
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/cart/all")
    public List<UserProduct> getUserProducts() {
        return productService.getUserProducts(UserUtils.getCurrentUser().getId());
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/cart/remove/{id}")
    public void removeUserProducts(@PathVariable int id) {
        productService.removeUserProducts(id, UserUtils.getCurrentUser().getId());
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/cart/payment")
    public void payment(@Valid @RequestBody ProductsPaymentDto productsPayment, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new FormIsNotValidException(bindingResult
                    .getFieldErrors().stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList()));
        }
        productService.payment(productsPayment, UserUtils.getCurrentUser().getId());
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/orders")
    public List<UserOrder> getOrders() {
        return productService.getOrders(UserUtils.getCurrentUser().getId());
    }
}
