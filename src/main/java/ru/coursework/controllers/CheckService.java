package ru.coursework.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/state")
public class CheckService {

    @GetMapping("/echo")
    public @ResponseBody String echo() {
        return "Hello, World!";
    }
}
