package ru.coursework.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.coursework.dto.*;
import ru.coursework.exceptions.FormIsNotValidException;
import ru.coursework.exceptions.UserNotExistException;
import ru.coursework.models.Authority;
import ru.coursework.models.User;
import ru.coursework.services.UserService;
import ru.coursework.utils.UserUtils;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController {

  private UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  @PostMapping("/sign_in")
  @PreAuthorize("isAnonymous()") public @ResponseBody
  UserLoginResponseDto signIn(@Valid @RequestBody UserLoginDto userLoginDTO, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new FormIsNotValidException(bindingResult
              .getFieldErrors().stream()
              .map(DefaultMessageSourceResolvable::getDefaultMessage)
              .collect(Collectors.toList()));
    }
    return userService.signIn(userLoginDTO.getEmail(), userLoginDTO.getPassword());
  }

  @PostMapping("/sign_up")
  @PreAuthorize("isAnonymous()")
  public void signUp(@Valid @RequestBody UserRegistrationDto userDto, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new FormIsNotValidException(bindingResult
              .getFieldErrors().stream()
              .map(DefaultMessageSourceResolvable::getDefaultMessage)
              .collect(Collectors.toList()));
    }
    userService.signUp(userDto, bindingResult);
  }

  @GetMapping("/me")
  @PreAuthorize("isAuthenticated()")
  public UserDto whoAmI() {
    User user = UserUtils.getCurrentUser();
    return UserDto.builder()
            .email(user.getEmail())
            .surname(user.getSurname())
            .name(user.getName())
            .authorities(user.getAuthorities())
            .build();
  }

  @GetMapping("/get")
  @PreAuthorize("hasAuthority('ADMIN')")
  public UserDto getUser(@RequestParam String email) {
    return userService.search(email);
  }

  @GetMapping(value = "/authorities")
  @PreAuthorize("hasAuthority('ADMIN')")
  public List<Authority> getAuthorities() {
    return userService.getAuthorities();
  }

  @PostMapping(value = "/authorities/add/{userId}/{authorityId}")
  @PreAuthorize("hasAuthority('ADMIN')")
  public void addUserAuthority(@PathVariable long userId, @PathVariable int authorityId) {
    userService.addUserAuthority(userId, authorityId);
  }

  @PostMapping(value = "/authorities/remove/{userId}/{authorityId}")
  @PreAuthorize("hasAuthority('ADMIN')")
  public void deleteUserAuthority(@PathVariable long userId, @PathVariable int authorityId) {
    userService.deleteUserAuthority(userId, authorityId);
  }

  @GetMapping(value = "/all")
  @PreAuthorize("hasAuthority('ADMIN')")
  public List<User> getAllUsers() {
    return userService.getAllUser();
  }

  @PostMapping(value = "/delete/{id}")
  @PreAuthorize("hasAuthority('ADMIN')")
  public void deleteUser(@PathVariable long id) {
    userService.deleteUser(id);
  }

  @PostMapping(value = "/reset/password")
  @PreAuthorize("isAnonymous()")
  public void resetPassword(@Valid @RequestBody UserPasswordResetDto userPasswordResetDto, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new FormIsNotValidException(bindingResult
              .getFieldErrors().stream()
              .map(DefaultMessageSourceResolvable::getDefaultMessage)
              .collect(Collectors.toList()));
    }
    userService.resetPassword(userPasswordResetDto);
  }

  @PostMapping(value = "/reset/password/code")
  @PreAuthorize("isAnonymous()")
  public void sendResetPassword(@RequestBody UserLoginDto userLoginDto) {
    User user = userService.getUser(userLoginDto.getEmail());
    if (user != null) {
      userService.sendResetPasswordCode(user);
    } else {
      throw new UserNotExistException();
    }
  }

  @PostMapping("/confirm")
  @PreAuthorize("isAnonymous()")
  public void confirmAccount(@Valid @RequestBody ActivationCodeDto activationCodeDto, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      throw new FormIsNotValidException(bindingResult
              .getFieldErrors().stream()
              .map(DefaultMessageSourceResolvable::getDefaultMessage)
              .collect(Collectors.toList()));
    }
    userService.confirm(activationCodeDto);
  }
}
