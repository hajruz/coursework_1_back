package ru.coursework.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.coursework.exceptions.FormIsNotValidException;

import java.util.List;

@ControllerAdvice
public class ExceptionHandlingController {

    @ExceptionHandler(FormIsNotValidException.class)
    public ResponseEntity<ExceptionResponse> formIsNotValid(FormIsNotValidException ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Bad request");
        response.setErrorMessages(ex.getMessages());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    public class ExceptionResponse {

        private String errorCode;
        private List<String> errorMessages;

        public ExceptionResponse() {}

        public String getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        public List<String> getErrorMessages() {
            return errorMessages;
        }

        public void setErrorMessages(List<String> errorMessages) {
            this.errorMessages = errorMessages;
        }
    }
}
