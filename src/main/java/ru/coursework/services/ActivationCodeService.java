package ru.coursework.services;

import ru.coursework.models.ActivationCode;

public interface ActivationCodeService {

    String generateCode();

    ActivationCode getConfirmationCodeByCode(String code);
    ActivationCode getConfirmActivationCode(long userId);
    ActivationCode getConfirmUserActivationCode(long userId);

    void deleteOldCode(int id);

    void deleteOldCodes();

    ActivationCode getByCode(String code);
    ActivationCode getResetPasswordCode(long userId);
    ActivationCode getUserResetPasswordCode(long userId);
}
