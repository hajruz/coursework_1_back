package ru.coursework.services;

import org.springframework.validation.BindingResult;
import ru.coursework.dto.*;
import ru.coursework.models.Authority;
import ru.coursework.models.User;

import java.util.List;

public interface UserService {
    UserLoginResponseDto signIn(String email, String password);

    void signUp(UserRegistrationDto userRegistrationDto, BindingResult bindingResult);

    User getUser(String email);

    List<Authority> getAuthorities();

    void addUserAuthority(long userId, int authorityId);

    void deleteUserAuthority(long userId, int authorityId);

    void confirm(ActivationCodeDto activationCodeDto);

    void sendConfirmCode(String email);

    void resetPassword(UserPasswordResetDto userPasswordResetDto);

    void sendResetPasswordCode(User currentUser);

    UserDto search(String email);

    List<User> getAllUser();

    void deleteUser(long userId);
}
