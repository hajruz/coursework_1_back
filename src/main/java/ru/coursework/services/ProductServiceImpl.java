package ru.coursework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ru.coursework.converters.DtoToProductConverter;
import ru.coursework.dto.ProductDto;
import ru.coursework.dto.ProductsPaymentDto;
import ru.coursework.exceptions.CustomException;
import ru.coursework.models.OrderProduct;
import ru.coursework.models.Product;
import ru.coursework.models.UserOrder;
import ru.coursework.models.UserProduct;
import ru.coursework.repositories.ProductRepository;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository;

    private DtoToProductConverter dtoToProductConverter;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository,
                              DtoToProductConverter dtoToProductConverter) {
        this.productRepository = productRepository;
        this.dtoToProductConverter = dtoToProductConverter;
    }

    @Override
    public Product getProduct(int id) {
        Product product = productRepository.read(id);
        if (product == null) {
            throw new CustomException("Not found 404", HttpStatus.NOT_FOUND);
        }
        return product;
    }

    @Override
    public List<Product> getProducts(boolean byPrice, int minPrice, int maxPrice) {
        if (minPrice > maxPrice || maxPrice < 0) {
            maxPrice = Integer.MAX_VALUE;
        }
        return productRepository.readAll(byPrice, minPrice, maxPrice);
    }

    @Override
    public int addProduct(ProductDto productDto) {
        return productRepository.create(dtoToProductConverter.convert(productDto));
    }

    @Override
    public void deleteProduct(int id) {
        productRepository.delete(id);
    }

    @Override
    public void addUserProduct(int productId, int count, long userId) {
        Product product = productRepository.read(productId);
        if (product != null) {
            if (count < 1 || count > 100) {
                count = 100;
            }

            UserProduct userProduct = productRepository.readUserProduct(productId, userId);
            if (userProduct == null) {
                userProduct = new UserProduct();
                userProduct.setId(productId);
                userProduct.setCount(count);
                userProduct.setUserId(userId);
                productRepository.create(userProduct);
            } else {
                userProduct.setCount(userProduct.getCount() + count);
                productRepository.updateUserProduct(userProduct);
            }
        } else {
            throw new CustomException("Not found 404", HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public List<UserProduct> getUserProducts(long userId) {
        return productRepository.readUsersProducts(userId);
    }

    @Override
    public void updateUserProduct(int id, int count, long userId) {
        UserProduct userProduct = productRepository.readUserProduct(id, userId);
        if (userProduct != null) {
            if (userProduct.getUserId() == userId) {
                if (count < 1 || count > 100) {
                    count = 1;
                }
                userProduct.setCount(count);
                productRepository.updateUserProduct(userProduct);
            } else {
                throw new CustomException("Access denied 403", HttpStatus.FORBIDDEN);
            }
        } else {
            throw new CustomException("Not found 404", HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public void removeUserProducts(int id, long userId) {
        UserProduct userProduct = productRepository.readUserProduct(id, userId);
        if (userProduct != null) {
            if (userProduct.getUserId() == userId) {
                productRepository.deleteUserProduct(userProduct.getUserProductId());
            } else {
                throw new CustomException("Access denied 403", HttpStatus.FORBIDDEN);
            }
        } else {
            throw new CustomException("Not found 404", HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public void payment(ProductsPaymentDto productsPayment, long userId) {
        for (UserProduct product : productsPayment.getProducts()) {
            if (!product.equals(productRepository.readUserProduct(product.getId(), userId))) {
                throw new CustomException("Оплата не удалась", HttpStatus.NOT_ACCEPTABLE);
            }
        }

        UserOrder userOrder = productRepository.createUserOrder(UserOrder.builder().userId(userId).address(productsPayment.getAddress()).build());

        for (UserProduct product : productsPayment.getProducts()) {
            productRepository.createOrderProduct(
                    OrderProduct
                            .builder()
                            .orderId(userOrder.getId())
                            .productId(product.getId())
                            .count(product.getCount())
                            .totalSum(product.getCount() * product.getPrice())
                            .name(product.getName())
                            .build());
            productRepository.deleteUserProduct(product.getUserProductId());
        }
    }

    @Override
    public List<UserOrder> getOrders(long userId) {
        return productRepository.readUserOrders(userId);
    }
}
