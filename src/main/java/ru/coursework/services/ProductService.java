package ru.coursework.services;

import ru.coursework.dto.ProductDto;
import ru.coursework.dto.ProductsPaymentDto;
import ru.coursework.models.Product;
import ru.coursework.models.UserOrder;
import ru.coursework.models.UserProduct;

import java.util.List;

public interface ProductService {

    Product getProduct(int id);
    List<Product> getProducts(boolean byPrice, int minPrice, int maxPrice);

    int addProduct(ProductDto productDto);

    void deleteProduct(int id);

    void addUserProduct(int productId, int count, long userId);

    List<UserProduct> getUserProducts(long userId);

    void updateUserProduct(int id, int count, long userId);

    void removeUserProducts(int id, long userId);

    void payment(ProductsPaymentDto productsPayment, long userId);

    List<UserOrder> getOrders(long id);
}
