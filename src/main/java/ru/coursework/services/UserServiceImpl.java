package ru.coursework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import ru.coursework.converters.DtoToUserConverter;
import ru.coursework.converters.RegistrationDtoToUserConverter;
import ru.coursework.converters.UserToDtoConverter;
import ru.coursework.dto.*;
import ru.coursework.exceptions.*;
import ru.coursework.models.ActivationCode;
import ru.coursework.models.Authority;
import ru.coursework.models.User;
import ru.coursework.repositories.UserRepository;
import ru.coursework.security.JwtTokenProvider;
import ru.coursework.utils.UserUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

  private PasswordEncoder passwordEncoder;
  private JwtTokenProvider jwtTokenProvider;
  private AuthenticationManager authenticationManager;

  private UserRepository userRepository;

  private DtoToUserConverter dtoToUserConverter;
  private UserToDtoConverter userToDtoConverter;
  private RegistrationDtoToUserConverter registrationDtoToUserConverter;

  private ActivationCodeService activationCodeService;
  private EmailService emailService;

  @Autowired
  public UserServiceImpl(UserRepository userRepository,
                         JwtTokenProvider jwtTokenProvider,
                         AuthenticationManager authenticationManager,
                         PasswordEncoder passwordEncoder,
                         DtoToUserConverter dtoToUserConverter,
                         UserToDtoConverter userToDtoConverter,
                         RegistrationDtoToUserConverter registrationDtoToUserConverter,
                         ActivationCodeService activationCodeService,
                         EmailService emailService) {
    this.userRepository = userRepository;
    this.jwtTokenProvider = jwtTokenProvider;
    this.authenticationManager = authenticationManager;
    this.passwordEncoder = passwordEncoder;
    this.dtoToUserConverter = dtoToUserConverter;
    this.userToDtoConverter = userToDtoConverter;
    this.registrationDtoToUserConverter = registrationDtoToUserConverter;
    this.activationCodeService = activationCodeService;
    this.emailService = emailService;
  }

  @Override
  public UserLoginResponseDto signIn(String email, String password) {
    UserLoginResponseDto userLoginResponseDTO = new UserLoginResponseDto();
    try {
      User user = userRepository.readByEmail(email);
      if (user == null || !user.isConfirmed()) {
        throw new ProfileNotConfirmedException();
      }
      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
      userLoginResponseDTO.setToken(jwtTokenProvider.createToken(email, user.getAuthorities()));

    } catch (AuthenticationException e) {
      throw new InvalidLoginDataException();
    }
    return  userLoginResponseDTO;
  }

  @Override
  public void signUp(UserRegistrationDto userRegistrationDto, BindingResult bindingResult) {
    User user = registrationDtoToUserConverter.convert(userRegistrationDto);
    if (user == null) {
      return;
    }

    if (userRepository.readByEmail(user.getEmail()) == null) {
      user.setPassword(passwordEncoder.encode(user.getPassword()));
      userRepository.create(user);
      sendConfirmCode(user.getEmail());
    } else {
      throw new UserAlreadyExistException();
    }
  }

  @Override
  public User getUser(String email) {
    return userRepository.readByEmail(email);
  }

  @Override
  public List<Authority> getAuthorities() {
    return userRepository.readAllAuthorities();
  }

  @Override
  public void addUserAuthority(long userId, int authorityId) {
    User user = userRepository.readById(userId);
    boolean alreadyContain = false;
    if (user != null) {
      for (Authority authority : user.getAuthorities()) {
        if (authority.getId() == authorityId) {
          alreadyContain = true;
          break;
        }
      }
    }
    if (user != null && !alreadyContain
            && userRepository.readAuthority(authorityId) != null) {
      userRepository.createUserAuthority(userId, authorityId);
    } else if (user == null){
      throw new CustomException("The user doesn't exist with id - " + userId, HttpStatus.NOT_FOUND);
    } else if (userRepository.readAuthority(authorityId) == null){
      throw new CustomException("The authority doesn't exist with id - " + authorityId, HttpStatus.NOT_FOUND);
    } else {
      throw new CustomException("The authority already exist with id - " + authorityId, HttpStatus.NOT_FOUND);
    }
  }

  @Override
  public void deleteUserAuthority(long userId, int authorityId) {
    if (userRepository.readById(userId) != null
            && userRepository.readAuthority(authorityId) != null) {
      userRepository.deleteUserAuthority(userId, authorityId);
    } else if (userRepository.readById(userId) == null){
      throw new CustomException("The user doesn't exist with id - " + userId, HttpStatus.NOT_FOUND);
    } else {
      throw new CustomException("The authority doesn't exist with id - " + authorityId, HttpStatus.NOT_FOUND);
    }
  }

  @Override
  public void confirm(ActivationCodeDto activationCodeDto) {
    ActivationCode activationCode = activationCodeService.getConfirmationCodeByCode(activationCodeDto.getCode());
    if (activationCode != null && activationCode.getCode().equals(activationCodeDto.getCode())) {
      userRepository.createUserAuthority(activationCode.getUserId(), new Authority(Authority.USER));
      userRepository.updateUserConfirmation(activationCode.getUserId(), true);
      activationCodeService.deleteOldCode(activationCode.getId());
    } else {
      throw new ActivationCodeIsInvalidException();
    }
  }

  @Override
  public void sendConfirmCode(String email) {
    User currentUser = userRepository.readByEmail(email);
    ActivationCode existActivationCode = activationCodeService.getConfirmUserActivationCode(currentUser.getId());
    if (!UserUtils.hasAuthority(currentUser.getAuthorities(), Authority.USER)
            && existActivationCode == null) {
      String code = activationCodeService.getConfirmActivationCode(currentUser.getId()).getCode();
      if (code == null) {
        throw new ActivationCodeGenerateException();
      }

      SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
      simpleMailMessage.setTo(currentUser.getEmail());
      simpleMailMessage.setSubject("Activation code:");
      simpleMailMessage.setText("Your confirmation code: https://coursework-front.herokuapp.com/profile/confirm/"
              + code + "\nIf you did not request confirmation code from our site ignore this message.");
      emailService.sendSimpleMessage(simpleMailMessage);
    } else if(existActivationCode != null) {
      throw new ActivationCodeExistException();
    }
  }

  @Override
  public void resetPassword(UserPasswordResetDto userPasswordResetDto) {
    ActivationCode activationCode = activationCodeService.getByCode(userPasswordResetDto.getResetCode());
    if (activationCode != null && activationCode.getCode().equals(userPasswordResetDto.getResetCode())) {
      userRepository.updatePassword(activationCode.getUserId(), passwordEncoder.encode(userPasswordResetDto.getPassword()));
      activationCodeService.deleteOldCode(activationCode.getId());
    } else {
      throw new ActivationCodeIsInvalidException();
    }
  }

  @Override
  public void sendResetPasswordCode(User currentUser) {
    ActivationCode existActivationCode = activationCodeService.getUserResetPasswordCode(currentUser.getId());
    if (existActivationCode == null) {
      String code = activationCodeService.getResetPasswordCode(currentUser.getId()).getCode();
      if (code == null) {
        throw new ActivationCodeGenerateException();
      }

      SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
      simpleMailMessage.setTo(currentUser.getEmail());
      simpleMailMessage.setSubject("Reset code:");
      simpleMailMessage.setText("Your reset password link: https://coursework-front.herokuapp.com/reset/password/"
              + code + "\nIf you did not request confirmation code from our site ignore this message.");
      emailService.sendSimpleMessage(simpleMailMessage);
    } else {
      throw new ActivationCodeExistException();
    }
  }

  @Override
  public UserDto search(String email) {
    User user = getUser(email);
    if (user == null) {
      throw new UserNotExistException();
    }
    return userToDtoConverter.convert(getUser(email));
  }

  @Override
  public List<User> getAllUser() {
    List<User> users = userRepository.readAll();
    for (User user : users) {
      user.setPassword("none");
    }
    return users;
  }

  @Override
  public void deleteUser(long userId) {
    if (userRepository.readById(userId) != null) {
      userRepository.deleteUser(userId);
    } else {
      throw new UserNotExistException();
    }
  }
}
