package ru.coursework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.coursework.models.ActivationCode;
import ru.coursework.repositories.ActivationCodeRepository;
import ru.coursework.utils.CodeGenerator;
import ru.coursework.utils.DateUtils;

import java.util.Objects;

@Service
public class ActivationCodeServiceImpl implements ActivationCodeService {

    public static final int GENERATING_ATTEMPTS = 5;

    private ActivationCodeRepository activationCodeRepository;

    @Autowired
    private ActivationCodeServiceImpl(ActivationCodeRepository activationCodeRepository) {
        this.activationCodeRepository = activationCodeRepository;
    }

    @Override
    public String generateCode() {
        String code;
        for (int i = 0; i < 5; i++) {
            code = CodeGenerator.generateKey(10,
                    CodeGenerator.ALPHA_CAPS
                            + CodeGenerator.ALPHA
                            + CodeGenerator.NUMERIC);
            if (Objects.isNull(activationCodeRepository.readByCode(code))) {
                return code;
            } else if (i == GENERATING_ATTEMPTS - 1){
                return null;
            }
        }
        return null;
    }

    @Override
    public ActivationCode getConfirmationCodeByCode(String code) {
        return activationCodeRepository.readByCode(code);
    }

    @Override
    public ActivationCode getConfirmActivationCode(long userId) {
        String code = generateCode();
        if (code == null) {
            return null;
        }

        ActivationCode activationCode = new ActivationCode();
        activationCode.setUserId(userId);
        activationCode.setType(ActivationCode.EMAIL_CONFIRM_CODE);
        activationCode.setCode(code);
        activationCode.setEndDate(DateUtils.getCurrentDate(ActivationCode.HALF_AN_HOUR));

        activationCodeRepository.create(activationCode);
        return activationCode;
    }

    @Override
    public ActivationCode getConfirmUserActivationCode(long userId) {
        ActivationCode activationCode = activationCodeRepository.readByUserAndType(userId, ActivationCode.EMAIL_CONFIRM_CODE);
        if (activationCode != null && activationCode.getEndDate().getTime() < DateUtils.getCurrentDate().getTime()) {
            deleteOldCode(activationCode.getId());
            return null;
        }
        return activationCode;
    }

    @Override
    public void deleteOldCode(int id){
        activationCodeRepository.delete(id);
    }

    @Override
    public void deleteOldCodes() {
        activationCodeRepository.deleteOldCodes();
    }

    @Override
    public ActivationCode getByCode(String code) {
        return activationCodeRepository.readByCode(code);
    }

    @Override
    public ActivationCode getResetPasswordCode(long userId) {
        String code = generateCode();
        if (code == null) {
            return null;
        }

        ActivationCode activationCode = new ActivationCode();
        activationCode.setUserId(userId);
        activationCode.setType(ActivationCode.PASSWORD_RESET_CODE);
        activationCode.setCode(code);
        activationCode.setEndDate(DateUtils.getCurrentDate(ActivationCode.HALF_AN_HOUR));

        activationCodeRepository.create(activationCode);
        return activationCode;
    }

    @Override
    public ActivationCode getUserResetPasswordCode(long userId) {
        ActivationCode activationCode = activationCodeRepository.readByUserAndType(userId, ActivationCode.PASSWORD_RESET_CODE);
        if (activationCode != null && activationCode.getEndDate().getTime() < DateUtils.getCurrentDate().getTime()) {
            deleteOldCode(activationCode.getId());
            return null;
        }
        return activationCode;
    }
}
