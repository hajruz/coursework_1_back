package ru.coursework.services;

import org.springframework.mail.SimpleMailMessage;

public interface EmailService {

    void sendSimpleMessage(SimpleMailMessage simpleMailMessage);

}
