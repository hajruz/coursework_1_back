package ru.coursework.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService{

    public static final String FROM = "no-reply@englisclub.com";

    private JavaMailSender javaMailSender;

    @Autowired
    public EmailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendSimpleMessage(SimpleMailMessage simpleMailMessage) {
        simpleMailMessage.setFrom(FROM);
        javaMailSender.send(simpleMailMessage);
    }
}
