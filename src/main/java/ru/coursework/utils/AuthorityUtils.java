package ru.coursework.utils;

import ru.coursework.exceptions.NotFoundException;
import ru.coursework.models.Authority;

public class AuthorityUtils {

    public static int getAuthorityId(Authority authority) {
        return getAuthorityId(authority.getAuthority());
    }

    public static int getAuthorityId(String authority){
        switch (authority){
            case "ADMIN": return 2;
            case "USER": return 1;
            default:
                try {
                    throw new NotFoundException("Authority \'" + authority + "\' not found");
                } catch (NotFoundException e) {
                    e.printStackTrace();
                }
                return -1;
        }
    }

    public static String getAuthorityTitle(int authorityId){
        switch (authorityId){
            case 1: return "USER";
            case 2: return "ADMIN";
            default:
                try {
                    throw new NotFoundException("Authority with id \'" + authorityId + "\' not found");
                } catch (NotFoundException e) {
                    e.printStackTrace();
                }
                return "-1";
        }
    }
}
