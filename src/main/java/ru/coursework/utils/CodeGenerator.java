package ru.coursework.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.security.SecureRandom;

public class CodeGenerator {

    public static final String ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String ALPHA = "abcdefghijklmnopqrstuvwxyz";
    public static final String NUMERIC = "0123456789";
    public static final String SPECIAL_CHARS = "!@#$%^&*_=+-/";

    private static SecureRandom random = new SecureRandom();

    public static String generateKey(int len, String symbols) {
        return RandomStringUtils.random(10, symbols);
    }

}
