package ru.coursework.utils;

import org.springframework.security.core.context.SecurityContextHolder;
import ru.coursework.models.Authority;
import ru.coursework.models.User;

import java.util.List;

public class UserUtils {

    public static User getCurrentUser(){
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public static boolean hasAuthority(List<Authority> authorities, String authority) {
        for (Authority _authority : authorities) {
            if (_authority.getAuthority().equals(authority)) {
                return true;
            }
        }
        return false;
    }

}
