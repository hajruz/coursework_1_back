package ru.coursework.utils;

import java.sql.Timestamp;

public class DateUtils {

    public static Timestamp getCurrentDate() {
        return new Timestamp(new java.util.Date().getTime());
    }

    public static Timestamp getCurrentDate(long delta) {
        return new Timestamp(new java.util.Date().getTime() + delta);
    }
}
