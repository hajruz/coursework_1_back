package ru.coursework.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.coursework.dto.UserRegistrationDto;
import ru.coursework.models.User;

@Component
public class RegistrationDtoToUserConverter implements Converter<UserRegistrationDto, User> {
    @Override
    public User convert(UserRegistrationDto userDto) {
        User user = new User();
        user.setEmail(userDto.getEmail());
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        user.setPassword(userDto.getPassword());
        return user;
    }
}
