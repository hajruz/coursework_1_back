package ru.coursework.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.coursework.dto.ProductDto;
import ru.coursework.models.Product;

@Component
public class DtoToProductConverter implements Converter<ProductDto, Product> {
    @Override
    public Product convert(ProductDto productDto) {
        return Product.builder()
                .name(productDto.getName())
                .description(productDto.getDescription())
                .price(productDto.getPrice())
                .imgURI(productDto.getImgURI())
                .imgFullURI(productDto.getImgFullURI())
                .build();
    }
}
