package ru.coursework.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.coursework.dto.UserDto;
import ru.coursework.models.User;

@Component
public class DtoToUserConverter implements Converter<UserDto, User> {
    @Override
    public User convert(UserDto userDto) {
        return User
                .builder()
                .id(userDto.getId())
                .email(userDto.getEmail())
                .name(userDto.getName())
                .surname(userDto.getSurname())
                .authorities(userDto.getAuthorities())
                .build();
    }
}
