package ru.coursework.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import ru.coursework.models.User;
import ru.coursework.repositories.UserRepository;

import java.util.stream.Collectors;

@Component
public class MyAuthenticationManager implements AuthenticationManager {

    private UserRepository userRepository;
    private BCryptPasswordEncoder encoder;

    @Autowired
    public MyAuthenticationManager(UserRepository userRepository, BCryptPasswordEncoder encoder){
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getPrincipal() + "";
        String password = authentication.getCredentials() + "";

        User user = userRepository.readByEmail(email);
        if (user == null) {
            throw new BadCredentialsException("1000");
        }
        if (!encoder.matches(password, user.getPassword())) {
            throw new BadCredentialsException("1000");
        }

        return new UsernamePasswordAuthenticationToken(email, password, user.getAuthorities().stream().map(x -> new SimpleGrantedAuthority(x.getAuthority())).collect(Collectors.toList()));
    }
}
