package ru.coursework.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginDto {

    @Size(min = 8, max = 50, message = "Длина E-mail не должна быть меньше 8 и не должна быть больше 50")
    private String email;

    @Size(min = 8, max = 64, message = "Длина пароля не должна быть меньше 8 и не должна быть больше 64")
    private String password;
}
