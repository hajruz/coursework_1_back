package ru.coursework.dto;

import lombok.*;
import ru.coursework.models.Authority;

import java.util.List;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

    private long id;
    private String email;
    private String surname;
    private String name;

    private List<Authority> authorities;

}
