package ru.coursework.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserLoginResponseDto {


    private String token;

    public UserLoginResponseDto(){

    }
}
