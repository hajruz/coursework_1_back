package ru.coursework.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserPasswordResetDto {

    @NotBlank
    private String resetCode;

    @Size(min = 8, max = 64, message = "Длина пароля не должна быть меньше 8 и не должна быть больше 64")
    private String password;
}
