package ru.coursework.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@Builder
@Getter
@NoArgsConstructor
@Setter
@ToString
@EqualsAndHashCode
public class ActivationCodeDto {

    @NotBlank(message = "Заполните поле")
    private String code;
}
