package ru.coursework.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.coursework.models.UserProduct;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class ProductsPaymentDto {

    @Pattern(regexp = "[0-9]*", message = "Номер карты содержит только цифры")
    @Size(min = 16, max = 16, message = "Введите номер карты")
    private String cardNumber;

    @Size(min = 3, max = 64, message = "Заполните поле с держателем от 3 до 64 символа")
    private String owner;

    @Pattern(regexp = "[0-9]*", message = "Код содержит только цифры")
    @Size(min = 3, max = 3, message = "Введите код")
    private String code;

    @Pattern(regexp = "[0-9]*", message = "Месяц содержит только цифры")
    @Size(min = 2, max = 2, message = "Введите месяц")
    private String month;

    @Pattern(regexp = "[0-9]*", message = "Год содержит только цифры")
    @Size(min = 4, max = 4, message = "Введите год")
    private String year;

    @Size(min = 5, max = 150, message = "Введите адрес доставки (от 5 до 150 символа)")
    private String address;

    private List<UserProduct> products;
}
