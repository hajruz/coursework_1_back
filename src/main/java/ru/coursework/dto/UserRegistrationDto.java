package ru.coursework.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRegistrationDto {

    @Size(min = 8, max = 50, message = "Длина E-mail не должна быть меньше 8 и не должна быть больше 50\n")
    private String email;

    @Pattern(regexp = "^[a-zA-Zа-яА-ЯёЁ]+$", message = "Имя содержит недопустимые символы\n")
    @Size(min = 3, max = 30, message = "Длина имени не должна быть меньше 5 и не должна быть больше 30\n")
    private String name;

    @Pattern(regexp = "^[a-zA-Zа-яА-ЯёЁ]+$", message = "Фамилия содержит недопустимые символы\n")
    @Size(min = 3, max = 50, message = "Длина фамилии не должна быть меньше 5 и не должна быть больше 50\n")
    private String surname;

    @Size(min = 8, max = 64, message = "Длина пароля не должна быть меньше 8 и не должна быть больше 64")
    private String password;
}
