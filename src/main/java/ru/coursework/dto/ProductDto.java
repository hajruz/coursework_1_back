package ru.coursework.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@AllArgsConstructor
@Builder
@Getter
@NoArgsConstructor
@Service
public class ProductDto {

    @Size(min = 8, max = 50, message = "Длина названия не должна быть меньше 2 и не должна быть больше 50")
    private String name;

    @Size(min = 8, max = 1000, message = "Длина описания не должна быть меньше 8 и не должна быть больше 1000")
    private String description;

    @Min(value = 1, message = "Стоимость не должена быть меньше, чем 1")
    @Max(value = 1000000, message = "Стоимость не должена быть больше, чем 1000000")
    private int price;

    @NotBlank(message = "Изображение не найдено")
    private String imgURI;

    @NotBlank(message = "Полное изображение не найдено")
    private String imgFullURI;

}
