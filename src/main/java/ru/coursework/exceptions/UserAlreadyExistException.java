package ru.coursework.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_ACCEPTABLE, reason = "Пользователь с таким E-mail уже существует")
public class UserAlreadyExistException extends RuntimeException{
}
