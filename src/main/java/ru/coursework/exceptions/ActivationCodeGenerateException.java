package ru.coursework.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Server can not generate key. Please, try again.")
public class ActivationCodeGenerateException extends RuntimeException {
}
