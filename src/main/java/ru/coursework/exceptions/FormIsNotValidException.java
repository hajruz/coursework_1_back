package ru.coursework.exceptions;

import java.util.List;

public class FormIsNotValidException extends RuntimeException {

    private List<String> messages;

    public FormIsNotValidException(List<String> messages) {
        this.messages = messages;
    }

    public FormIsNotValidException(String message) {
        super(message);
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}
