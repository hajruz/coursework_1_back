package ru.coursework.models;

import lombok.*;

import java.sql.Timestamp;

@AllArgsConstructor
@Builder
@Getter
@NoArgsConstructor
@Setter
@ToString
@EqualsAndHashCode
public class ActivationCode {

    public static final String EMAIL_CONFIRM_CODE = "EMAIL_CONFIRM";
    public static final String PASSWORD_RESET_CODE = "PASSWORD_RESET";

    public static final long HALF_AN_HOUR = 1800000L;

    private int id;
    private String code;
    private long userId;
    private Timestamp endDate;
    private String type;

}
