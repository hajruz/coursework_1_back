package ru.coursework.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class UserOrder {

    private int id;
    private long userId;
    private String address;

    private List<OrderProduct> products;
}
