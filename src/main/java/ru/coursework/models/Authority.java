package ru.coursework.models;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;

@Setter
@Getter
@Builder
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Authority implements GrantedAuthority {

  public static final String ADMIN = "ADMIN";
  public static final String USER = "USER";

  private int id;
  private String authority;

  public Authority(String authority){
    this.authority = authority;
  }

  @Override
  public String getAuthority() {
    return authority;
  }
}
