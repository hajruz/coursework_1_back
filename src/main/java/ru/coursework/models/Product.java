package ru.coursework.models;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Product {

    private int id;
    private String name;
    private int price;
    private String description;
    private String imgURI;
    private String imgFullURI;

}
