package ru.coursework.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
public class OrderProduct {

    private int orderId;
    private int productId;
    private int count;
    private int totalSum;
    private String name;

}
