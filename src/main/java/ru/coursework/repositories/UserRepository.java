package ru.coursework.repositories;

import ru.coursework.dto.UserDto;
import ru.coursework.models.Authority;
import ru.coursework.models.OrderProduct;
import ru.coursework.models.User;
import ru.coursework.models.UserOrder;

import java.util.List;

public interface UserRepository {

    void create(User user);

    void createUserAuthority(long userId, Authority authority);

    void createUserAuthority(long userId, int authorityId);


    User readByEmail(String email);

    List<Authority> readAuthorities(long userId);

    User readById(long id);

    List<Authority> readAllAuthorities();

    Authority readAuthority(int authorityId);

    void update(User user);

    void updateUserConfirmation(long id, boolean b);

    void updatePassword(long id, String password);

    void deleteUser(long userId);

    void deleteAllAuthorities(long userId);

    void deleteUserAuthority(long userId, int authorityId);


    void deleteNonActivatedAccounts();

    List<User> readAll();
}
