package ru.coursework.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.coursework.models.Authority;
import ru.coursework.models.User;
import ru.coursework.utils.AuthorityUtils;
import ru.coursework.utils.DateUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserRepositoryImpl implements UserRepository {

    public static final String CREATE_USER_SQL = "INSERT INTO users(name, surname, email, password, registration_date) VALUES (?, ?, ?, ?, ?)";
    public static final String CREATE_USER_AUTHORITY_SQL = "INSERT INTO users_autohorities(user_id, authority_id) VALUES(?, ?)";

    public static final String READ_USER_AUTHORITIES_SQL = "SELECT * FROM users_autohorities INNER JOIN authorities ON authority_id = id WHERE user_id=?;";
    public static final String READ_USERS_SQL = "SELECT * FROM users";
    public static final String READ_ALL_AUTHORITIES_SQL = "SELECT * FROM authorities;";
    public static final String READ_AUTHORITY_SQL = "SELECT * FROM authorities WHERE id = ?;";
    public static final String READ_USER_BY_EMAIL_SQL = "SELECT * FROM users WHERE email=?";
    public static final String READ_USER_BY_ID_SQL = "SELECT * FROM users WHERE id=?";

    public static final String UPDATE_USER_SQL = "UPDATE users SET name = ?, surname = ? WHERE id = ?";
    private static final String UPDATE_USER_CONFIRMATION_SQL = "UPDATE users SET confirmed = ? WHERE id = ?";
    private static final String UPDATE_USER_PASSWORD_SQL = "UPDATE users SET password = ? WHERE id = ?";

    public static final String DELETE_ALL_USER_AUTHORITIES_SQL = "DELETE FROM users_autohorities WHERE user_id = ?";
    public static final String DELETE_USER_SQL = "DELETE FROM users WHERE id = ?";
    public static final String DELETE_USER_AUTHORITIES_SQL = "DELETE FROM users_autohorities WHERE user_id = ? AND authority_id = ?";
    public static final String DELETE_NON_ACTIVATED_ACCOUNTS_SQL = "SELECT delete_non_activated_accounts()";
    private Connection connection;

    @Autowired
    public UserRepositoryImpl(DataSource dataSource) {
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void create(User user) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_USER_SQL);
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setTimestamp(5, DateUtils.getCurrentDate());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createUserAuthority(long userId, Authority authority) {
        createUserAuthority(userId, AuthorityUtils.getAuthorityId(authority));
    }

    @Override
    public void createUserAuthority(long userId, int authorityId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_USER_AUTHORITY_SQL);
            preparedStatement.setLong(1, userId);
            preparedStatement.setInt(2, authorityId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(User user) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_SQL);
            preparedStatement.setString(1, user.getUsername());
            preparedStatement.setString(2, user.getSurname());
            preparedStatement.setLong(3, user.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User readByEmail(String email) {
        User user = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(READ_USER_BY_EMAIL_SQL);
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
            {
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setConfirmed(resultSet.getBoolean("confirmed"));
                user.setAuthorities(readAuthorities(user.getId()));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public User readById(long id) {
        User user = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(READ_USER_BY_ID_SQL);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next())
            {
                user = User
                        .builder()
                        .id(resultSet.getInt("id"))
                        .name(resultSet.getString("name"))
                        .surname(resultSet.getString("surname"))
                        .email(resultSet.getString("email"))
                        .password(resultSet.getString("password"))
                        .confirmed(resultSet.getBoolean("confirmed"))
                        .authorities(readAuthorities(resultSet.getInt("id")))
                        .build();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public List<Authority> readAllAuthorities() {
        List<Authority> authorities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_AUTHORITIES_SQL);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                authorities.add(Authority
                        .builder()
                        .authority(resultSet.getString("role"))
                        .id(resultSet.getInt("id"))
                        .build());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return authorities;
    }

    @Override
    public Authority readAuthority(int authorityId) {
        Authority authority = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(READ_AUTHORITY_SQL);
            preparedStatement.setInt(1, authorityId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                authority = Authority
                        .builder()
                        .id(resultSet.getInt("id"))
                        .authority(resultSet.getString("role"))
                        .build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return authority;
    }


    @Override
    public List<Authority> readAuthorities(long userId){
        List<Authority> authorities = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(READ_USER_AUTHORITIES_SQL);
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                authorities.add(Authority
                        .builder()
                        .id(resultSet.getInt("authority_id"))
                        .authority(resultSet.getString("role"))
                        .build());
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return authorities;
    }

    @Override
    public void updateUserConfirmation(long userId, boolean state) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement(UPDATE_USER_CONFIRMATION_SQL);
            preparedStatement.setBoolean(1, state);
            preparedStatement.setLong(2, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updatePassword(long userId, String password) {
        PreparedStatement preparedStatement;
        try {
            preparedStatement = connection.prepareStatement(UPDATE_USER_PASSWORD_SQL);
            preparedStatement.setString(1, password);
            preparedStatement.setLong(2, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteUser(long userId){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER_SQL);
            preparedStatement.setLong(1, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAllAuthorities(long userId){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ALL_USER_AUTHORITIES_SQL);
            preparedStatement.setLong(1, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteUserAuthority(long userId, int authorityId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER_AUTHORITIES_SQL);
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, authorityId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteNonActivatedAccounts() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NON_ACTIVATED_ACCOUNTS_SQL);
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> readAll() {
        List<User> users = new ArrayList<>();
        User user;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(READ_USERS_SQL);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next())
            {
                user = new User();
                user.setId(resultSet.getInt("id"));
                user.setName(resultSet.getString("name"));
                user.setSurname(resultSet.getString("surname"));
                user.setEmail(resultSet.getString("email"));
                user.setPassword(resultSet.getString("password"));
                user.setConfirmed(resultSet.getBoolean("confirmed"));
                user.setAuthorities(readAuthorities(user.getId()));
                users.add(user);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }
}
