package ru.coursework.repositories;

import ru.coursework.models.ActivationCode;

public interface ActivationCodeRepository {

    void create(ActivationCode activationCode);
    ActivationCode read(int id);
    ActivationCode readByCode(String code);
    ActivationCode readByUserAndType(long userId, String type);

    void update(ActivationCode activationCode);
    void delete(int id);

    void deleteOldCodes();
}
