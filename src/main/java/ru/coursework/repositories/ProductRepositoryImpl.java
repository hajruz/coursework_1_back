package ru.coursework.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.coursework.models.OrderProduct;
import ru.coursework.models.Product;
import ru.coursework.models.UserOrder;
import ru.coursework.models.UserProduct;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

    public static final String CREATE_PRODUCT_SQL = "INSERT INTO products(name, description, price, image_uri, full_image_uri) VALUES(?, ?, ?, ?, ?)";
    public static final String CREATE_USER_PRODUCT_SQL = "INSERT INTO users_products(product_id, count, user_id) VALUES(?, ?, ?)";
    public static final String CREATE_USER_ORDER_SQL = "INSERT INTO users_orders(user_id, address) VALUES(?, ?)";
    public static final String CREATE_ORDER_PRODUCT_SQL = "INSERT INTO orders_products(order_id, product_id, count, total_sum, name) VALUES (?, ?, ?, ?, ?)";

    public static final String READ_PRODUCT_BY_ID_SQL = "SELECT * FROM products WHERE id = ?";
    public static final String READ_PRODUCT_ALL_SQL = "SELECT * FROM products WHERE price BETWEEN ? AND ? ORDER BY id";
    public static final String READ_PRODUCT_ALL_BY_PRICE_SQL = "SELECT * FROM products WHERE price BETWEEN ? AND ? ORDER BY price";
    public static final String READ_USER_PRODUCT_SQL = "SELECT * FROM (SELECT * FROM users_products WHERE product_id = ? AND user_id = ?) as user_products INNER JOIN products ON product_id = products.id";
    public static final String READ_USERS_PRODUCTS_SQL = "SELECT * FROM (SELECT * FROM users_products WHERE user_id = ?) as user_products INNER JOIN products ON product_id = products.id";
    public static final String READ_USER_ORDERS_SQL = "SELECT * FROM users_orders WHERE user_id = ?";
    public static final String READ_ORDER_PRODUCTS_SQL = "SELECT * FROM orders_products WHERE order_id = ?";

    public static final String UPDATE_USER_PRODUCT_SQL = "UPDATE users_products SET count = ? WHERE user_id = ? AND id = ?";

    public static final String DELETE_PRODUCT_SQL = "DELETE FROM products WHERE id = ?";
    public static final String DELETE_USER_PRODUCT_SQL = "DELETE FROM users_products WHERE id = ?";

    private Connection connection;

    @Autowired
    public ProductRepositoryImpl(DataSource dataSource) {
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int create(Product product) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_PRODUCT_SQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, product.getName());
            preparedStatement.setString(2, product.getDescription());
            preparedStatement.setInt(3, product.getPrice());
            preparedStatement.setString(4, product.getImgURI());
            preparedStatement.setString(5, product.getImgFullURI());
            preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                return resultSet.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    @Override
    public void create(UserProduct product) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_USER_PRODUCT_SQL);
            preparedStatement.setInt(1, product.getId());
            preparedStatement.setInt(2, product.getCount());
            preparedStatement.setLong(3, product.getUserId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public UserOrder createUserOrder(UserOrder userOrder) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_USER_ORDER_SQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setLong(1, userOrder.getUserId());
            preparedStatement.setString(2, userOrder.getAddress());
            preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                userOrder.setId(resultSet.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userOrder;
    }

    @Override
    public void createOrderProduct(OrderProduct orderProduct) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_ORDER_PRODUCT_SQL);
            preparedStatement.setInt(1, orderProduct.getOrderId());
            preparedStatement.setInt(2, orderProduct.getProductId());
            preparedStatement.setInt(3, orderProduct.getCount());
            preparedStatement.setInt(4, orderProduct.getTotalSum());
            preparedStatement.setString(5, orderProduct.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Product read(int id) {
        Product product = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(READ_PRODUCT_BY_ID_SQL);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                product = Product.builder()
                        .id(resultSet.getInt("id"))
                        .name(resultSet.getString("name"))
                        .price(resultSet.getInt("price"))
                        .description(resultSet.getString("description"))
                        .imgURI(resultSet.getString("image_uri"))
                        .imgFullURI(resultSet.getString("full_image_uri"))
                        .build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return product;
    }

    @Override
    public List<Product> readAll(boolean byPrice, int minPrice, int maxPrice) {
        List<Product> products = new ArrayList<>();
        try {
            PreparedStatement preparedStatement;
            if (byPrice) {
                preparedStatement = connection.prepareStatement(READ_PRODUCT_ALL_BY_PRICE_SQL);
            } else {
                preparedStatement = connection.prepareStatement(READ_PRODUCT_ALL_SQL);
            }
            preparedStatement.setInt(1, minPrice);
            preparedStatement.setInt(2, maxPrice);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                products.add(Product.builder()
                        .id(resultSet.getInt("id"))
                        .name(resultSet.getString("name"))
                        .price(resultSet.getInt("price"))
                        .description(resultSet.getString("description"))
                        .imgURI(resultSet.getString("image_uri"))
                        .build());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return products;
    }

    @Override
    public UserProduct readUserProduct(int productId, long userId) {
        UserProduct product = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(READ_USER_PRODUCT_SQL);

            preparedStatement.setInt(1, productId);
            preparedStatement.setLong(2, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                product = new UserProduct();
                product.setId(resultSet.getInt("product_id"));
                product.setName(resultSet.getString("name"));
                product.setPrice(resultSet.getInt("price"));
                product.setDescription(resultSet.getString("description"));
                product.setImgURI(resultSet.getString("image_uri"));
                product.setCount(resultSet.getInt("count"));
                product.setUserProductId(resultSet.getInt("id"));
                product.setUserId(resultSet.getInt("user_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return product;
    }

    @Override
    public List<UserProduct> readUsersProducts(long userId) {
        List<UserProduct> products = new ArrayList<>();
        UserProduct product;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(READ_USERS_PRODUCTS_SQL);

            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                product = new UserProduct();
                product.setId(resultSet.getInt("product_id"));
                product.setName(resultSet.getString("name"));
                product.setPrice(resultSet.getInt("price"));
                product.setDescription(resultSet.getString("description"));
                product.setImgURI(resultSet.getString("image_uri"));
                product.setCount(resultSet.getInt("count"));
                product.setUserProductId(resultSet.getInt("id"));
                product.setUserId(resultSet.getInt("user_id"));
                products.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return products;
    }

    @Override
    public List<UserOrder> readUserOrders(long userId) {
        List<UserOrder> orders = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(READ_USER_ORDERS_SQL);
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                UserOrder order = UserOrder
                        .builder()
                        .id(resultSet.getInt("id"))
                        .userId(resultSet.getInt("user_id"))
                        .address(resultSet.getString("address"))
                        .build();
                order.setProducts(readOrderProducts(order.getId()));
                orders.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }

    @Override
    public List<OrderProduct> readOrderProducts(int orderId) {
        List<OrderProduct> products = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(READ_ORDER_PRODUCTS_SQL);

            preparedStatement.setLong(1, orderId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                products.add(OrderProduct
                        .builder()
                        .orderId(resultSet.getInt("order_id"))
                        .productId(resultSet.getInt("product_id"))
                        .count(resultSet.getInt("count"))
                        .totalSum(resultSet.getInt("total_sum"))
                        .name(resultSet.getString("name"))
                        .build());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return products;
    }

    @Override
    public void updateUserProduct(UserProduct product) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_PRODUCT_SQL);
            preparedStatement.setInt(1, product.getCount());
            preparedStatement.setLong(2, product.getUserId());
            preparedStatement.setInt(3, product.getUserProductId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_PRODUCT_SQL);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteUserProduct(int userProductId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USER_PRODUCT_SQL);
            preparedStatement.setInt(1, userProductId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
