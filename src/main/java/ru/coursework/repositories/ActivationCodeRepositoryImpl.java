package ru.coursework.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.coursework.models.ActivationCode;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class ActivationCodeRepositoryImpl implements ActivationCodeRepository{

    public static final String CREATE_SQL = "INSERT INTO activation_codes(code, user_id, end_date, type) VALUES (?, ?, ?, ?)";

    public static final String READ_SQL = "SELECT * FROM activation_codes WHERE id = ?";
    public static final String READ_BY_CODE_SQL = "SELECT * FROM activation_codes WHERE code = ?";
    public static final String READ__BY_USER_ID_AND_TYPE_SQL = "SELECT * FROM activation_codes WHERE user_id = ? AND type = ?";

    public static final String UPDATE_SQL = "UPDATE activation_codes SET code = ?, user_id = ?, end_date = ?, type = ? WHERE id = ?";

    private static final String DELETE_SQL = "DELETE FROM activation_codes WHERE id = ?";

    private static final String CALL_FUNC_DELETE_OLD_CODES_SQL = "SELECT delete_old_activation_codes();";

    private Connection connection;

    @Autowired
    public ActivationCodeRepositoryImpl(DataSource dataSource) {
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void create(ActivationCode activationCode) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_SQL);
            preparedStatement.setString(1, activationCode.getCode());
            preparedStatement.setLong(2, activationCode.getUserId());
            preparedStatement.setTimestamp(3, activationCode.getEndDate());
            preparedStatement.setString(4, activationCode.getType());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ActivationCode read(int id) {
        ActivationCode activationCode = null;
        try {
            PreparedStatement preparedStatement = preparedStatement = connection.prepareStatement(READ_SQL);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                activationCode = ActivationCode
                        .builder()
                        .id(resultSet.getInt("id"))
                        .code(resultSet.getString("code"))
                        .userId(resultSet.getLong("user_id"))
                        .endDate(resultSet.getTimestamp("end_date"))
                        .type(resultSet.getString("type"))
                        .build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return activationCode;
    }

    @Override
    public ActivationCode readByCode(String code) {
        ActivationCode activationCode = null;
        try {
            PreparedStatement preparedStatement = preparedStatement = connection.prepareStatement(READ_BY_CODE_SQL);
            preparedStatement.setString(1, code);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                activationCode = ActivationCode
                        .builder()
                        .id(resultSet.getInt("id"))
                        .code(resultSet.getString("code"))
                        .userId(resultSet.getLong("user_id"))
                        .endDate(resultSet.getTimestamp("end_date"))
                        .type(resultSet.getString("type"))
                        .build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return activationCode;
    }

    @Override
    public ActivationCode readByUserAndType(long userId, String type) {
        ActivationCode activationCode = null;
        try {
            PreparedStatement preparedStatement = preparedStatement = connection.prepareStatement(READ__BY_USER_ID_AND_TYPE_SQL);
            preparedStatement.setLong(1, userId);
            preparedStatement.setString(2, type);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                activationCode = ActivationCode
                        .builder()
                        .id(resultSet.getInt("id"))
                        .code(resultSet.getString("code"))
                        .userId(resultSet.getLong("user_id"))
                        .endDate(resultSet.getTimestamp("end_date"))
                        .type(resultSet.getString("type"))
                        .build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return activationCode;
    }

    @Override
    public void update(ActivationCode activationCode) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SQL);
            preparedStatement.setString(1, activationCode.getCode());
            preparedStatement.setLong(2, activationCode.getUserId());
            preparedStatement.setTimestamp(3, activationCode.getEndDate());
            preparedStatement.setString(4, activationCode.getType());
            preparedStatement.setInt(5, activationCode.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SQL);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteOldCodes() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(CALL_FUNC_DELETE_OLD_CODES_SQL);
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
