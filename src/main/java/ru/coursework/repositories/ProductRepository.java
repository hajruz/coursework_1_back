package ru.coursework.repositories;

import ru.coursework.models.OrderProduct;
import ru.coursework.models.Product;
import ru.coursework.models.UserOrder;
import ru.coursework.models.UserProduct;

import java.util.List;

public interface ProductRepository {

    int create(Product product);

    void create(UserProduct product);

    UserOrder createUserOrder(UserOrder userOrder);

    void createOrderProduct(OrderProduct orderProduct);

    Product read(int id);

    List<Product> readAll(boolean byPrice, int minPrice, int maxPrice);

    UserProduct readUserProduct(int productId, long userId);

    List<UserProduct> readUsersProducts(long userId);

    List<UserOrder> readUserOrders(long userId);

    List<OrderProduct> readOrderProducts(int productId);

    void updateUserProduct(UserProduct product);

    void delete(int id);

    void deleteUserProduct(int userProductId);

}
